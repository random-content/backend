package dev.wg.randomcontentbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RandomContentBackendApplication

fun main(args: Array<String>) {
    runApplication<RandomContentBackendApplication>(*args)
}
